<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Curso;

class CursoController extends Controller
{
    public function index(){
    	$registros = Curso::all();
    	return view('admin.cursos.index', compact('registros'));
    }
    public function adicionar(){
    	return view('admin.cursos.adicionar');
    }
    //metodo para pegar a requisição REQUEST
    public function salvar(Request $req){
    	$dados = $req->all();

    	if (isset($dados['publicado'])){
    		$dados['publicado']='sim';
    	}else{
    		$dados['publicado'] = 'nao';
    	}

    	if($req->hasFile('imagem')){
    		$imagem = $req->file('imagem');
    		$num = rand(1111,9999);
    		$dir = "img/cursos/";
    		$extensao = $imagem->guessClientExtension();
    		$nomeImagem = "imagem_".$num.".".$extensao;
    		$imagem->move($dir,$nomeImagem);
    		$dados["imagem"] = $dir."/".$nomeImagem;
    	}
    	curso::create($dados);
    	return redirect()->route('admin.cursos');
    }
    public function editar($id){
    	$registro = Curso::find($id);
    	return view('admin..cursos.editar',compact('registro'));
    }
    public function atualizar(Request $req, $id){
    	$dados = $req->all();

    	if (isset($dados['publicado'])){
    		$dados['publicado']='sim';
    	}else{
    		$dados['publicado'] = 'nao';
    	}

    	if($req->hasFile('imagem')){
    		$imagem = $req->file('imagem');
    		$num = rand(1111,9999);
    		$dir = "img/cursos/";
    		$extensao = $imagem->guessClientExtension();
    		$nomeImagem = "imagem_".$num.".".$extensao;
    		$imagem->move($dir,$nomeImagem);
    		$dados["imagem"] = $dir."/".$nomeImagem;
    	}
    	//buscando o registro pelo id FIND()
    	curso::find($id)->update($dados);
    	return redirect()->route('admin.cursos');
    }
    public function deletar($id){
    	curso::find($id)->delete();
    	return redirect()->route('admin.cursos');
    }
}
