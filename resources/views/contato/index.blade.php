<!--extendendo o html do view/layout/site-->
@extends('layout.site')
<!-- é aonde o conteúdo vai ser exibido na template onde está yield-->
<!-- para mudar o titulo na variavel('atual','novo')-->
@section('titulo', 'contato')

<!--colocando dentro do ('conteudo')-->
@section('conteudo')
	<h1>Essa e o index do contato</h1>
	<!--lista de contatos ai ser inserida em contato pelo loop-->
	@foreach($contatos as $contato)
		<p>{{ $contato->nome }}</p>
		<p>{{ $contato->tel }}</p>
	@endforeach
@endsection