<!--incluindo o cabecalho-->
@include('layout._includes.topo')

<!--utilizada simplesmente como passagem de valor-->
@yield('conteudo')

<!--incluindo o rodape-->
@include('layout._includes.footer')
